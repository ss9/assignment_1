import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by shadman on 11/2/17.
 */
public class LogParser {
    //this will hold the name of the file to be parsed
    private String fileName;
    //this will hold the entries for all times of the day
    Entry[] entries;

    //constructor
    LogParser(String fileName) {
        this.fileName = fileName;
        this.entries = new Entry[24];
        for (int i = 0; i < 24; i++) {
            this.entries[i] = new Entry(i);
        }
    }

    public static void main(String[] args) {
        //take a new parser to process the given file
        LogParser lp = new LogParser("03 - sample-log-file.log");
        //parse the file
        lp.parse();
        //in case of java LogParser<FILENAME> --sort
        if (args.length == 1 && args[0].equals("--sort")) {
            lp.sortedPrint();
        }
        //in case of java LogParser<FILENAME>
        else {
            lp.print();
        }
    }

    public void parse() {
        //this will hold the individual lines of the file
        ArrayList<String> list = new ArrayList<String>();
        //populate the list
        readFileIntoList(this.fileName, list);
        for (String s : list) {
            //tokenize the string with space as delimiter
            String[] line = s.split(" ");
            //extract the useful information from this line and keep updating entries
            extractInfo(line);
        }
    }

    private boolean readFileIntoList(String fileName, ArrayList<String> list) {
        File file = null;
        FileReader fr = null;
        BufferedReader br = null;
        //file handling can cause a few Exceptions
        try {
            file = new File(fileName);
            fr = new FileReader(file);
            br = new BufferedReader(fr);
            String s;
            //take a line into s and add the line to the list
            while ((s = br.readLine()) != null) {
                list.add(s);
            }
            return true;
        }
        //if something goes wrong
        catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
        //close resources
        finally {
            //fr.close();
            //br.close();
        }
    }

    private void extractInfo(String[] line) {
        //useful attributes are time, get/post count, unique uri counts and total response time
        //get the time portion of the request first
        String timePortion = line[1];
        //get the hour from the time
        Integer hour = new Integer(timePortion.split(":")[0]);
        //retrieve the request type (get/post)
        Character type = line[line.length - 2].charAt(0);
        //if it is not get/post, ignore
        if (type == 'G' || type == 'P') {
            //save the response time portion of the line
            String trtPortion = line[line.length - 1];
            //the actual time will begin at index 5 and end just before the start of "ms" as trtPortion holds a value like time=xxxms
            Integer trt = new Integer(trtPortion.substring(5, trtPortion.indexOf("ms")));
            //retrieve the URI portion of the line
            String uriPortion = line[line.length - 3];
            //get the actual uri
            String uri = uriPortion.substring(uriPortion.indexOf('['), uriPortion.indexOf(']') + 1);
            //now update the members of this entry
            if (type == 'G') {
                this.entries[hour].incGets();
            } else if (type == 'P') {
                this.entries[hour].incPosts();
            }
            this.entries[hour].addUri(uri);
            this.entries[hour].incTotalTime(trt);
        }
    }

    public void sortedPrint() {
        //comparator that compares entries
        EntryComparator ec = new EntryComparator();
        //have to send the comparator too
        Arrays.sort(this.entries, ec);
        //now that the entries are sorted, call print
        print();
    }

    public void print() {
        //unformatted, uses tostring()
        /*for(Entry e: this.entries){
            System.out.println(e);
        }*/
        //formatted, uses getters
        System.out.printf("%20s%30s%30s%30s\n", "TIME", "GET/POST Count", "Unique URI Count", "Total Response Time");
        for (int i = 0; i < 24; i++) {
            //save the current entry
            Entry e = this.entries[i];
            //this will hold the time in (from - to) format
            String t = Helper.formatDate(e.getStartTime()) + " - " + Helper.formatDate(e.getEndTime());
            //second column
            String getPostCount = e.getGets() + "/" + e.getPosts();
            //third column
            String uniqueUriCount = e.getUniqueUris().toString();
            //fourth column
            String totalresponseTime = e.getTotalTime() + " ms";
            //output the formatted string
            System.out.printf("%20s%30s%30s%30s\n", t, getPostCount, uniqueUriCount, totalresponseTime);
        }
    }
}

class EntryComparator implements Comparator<Entry> {
    //first sort by get requests, if equal, then by post requests
    public int compare(Entry e1, Entry e2) {
        if (e2.getGets() == e1.getGets()) {
            return e2.getPosts().compareTo(e1.getPosts());
        }
        return e2.getGets().compareTo(e1.getGets());
    }
}

class Entry {
    //save the start and end times
    private Integer startTime, endTime;
    //this will contain unique uri's only
    private HashSet<String> uris;
    //get/post counts and total time
    private int gets;
    private int posts;
    private int totalTime;

    //constructor
    Entry(Integer startTime) {
        this.startTime = startTime;
        this.endTime = startTime + 1;
        this.uris = new HashSet<String>();
        this.gets = 0;
        this.posts = 0;
        this.totalTime = 0;
    }

    //getters
    public Integer getStartTime() {
        return this.startTime;
    }

    public Integer getEndTime() {
        return this.endTime;
    }

    public Integer getGets() {
        return this.gets;
    }

    public Integer getPosts() {
        return this.posts;
    }

    public Integer getUniqueUris() {
        return this.uris.size();
    }

    public Integer getTotalTime() {
        return this.totalTime;
    }

    //mutators
    public void incGets() {
        this.gets++;
    }

    public void incPosts() {
        this.posts++;
    }

    public void incTotalTime(int amount) {
        this.totalTime += amount;
    }

    public boolean addUri(String uri) {
        return uris.add(uri);
    }

    //required if entries need to be stored in hashtables or maps, not used here though
    public boolean equals(Object o) {
        if (o instanceof Entry) {
            Entry e = (Entry) o;
            return e.startTime.equals(this.startTime);
        }
        return false;
    }

    public int hashCode() {
        return this.startTime;
    }

    //toString returns the object's string representation in an unformatted way
    public String toString() {
        return Helper.formatDate(this.startTime) + " - " + Helper.formatDate(this.endTime) + " " + this.gets + "/" + this.posts + " " + this.uris.size() + " " + this.totalTime + "ms";
    }
}

//helper class that would contain several necessary methods
class Helper {
    public static String formatDate(Integer t) {
        //if hour<12 or hour==24, then it's am
        String amorpm = (t >= 12 && t < 24) ? "pm" : "am";
        //12 hour time from 24 hour time
        t %= 12;
        //0 hour should be written as 12 hour
        t = (t == 0 ? 12 : t);
        //if hour<10 then add a 0 in the front, add .00 after the hour and am/pm at the end
        return (t < 10 ? "0" : "") + t + ".00 " + amorpm;
    }
}